<?php
/**
 * Created by PhpStorm.
 * User: Sammy
 * Date: 03/01/2017
 * Time: 19:21
 */
namespace Pixasia\Notification;

use GuzzleHttp\Client;

class Factory
{

    /**
     * @param string $factory The name of the factory to create
     * @param array [string] $data An array of data to initialise the object with
     * @param \GuzzleHttp\Client $client The client to used
     *
     * @return \Pixasia\Notification\GatewayInterface
     *
     * @throws \Exception
     */
    public static function create($factory, $data = [], $client = null)
    {
        $factory = self::normalise($factory);

        if (!class_exists($factory)) {
            throw new \Exception('Invalid class');
        }

        return new $factory($data, $client);
    }

    /**
     * Normalise the Factory name
     *
     * @param string $factory
     *
     * @return string
     */
    private static function normalise($factory)
    {
        return '\\Pixasia\\Notification\\Gateway\\' . ucfirst(strtolower($factory));
    }
}