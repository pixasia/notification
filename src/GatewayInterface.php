<?php
/**
 * Created by PhpStorm.
 * User: Sammy
 * Date: 03/01/2017
 * Time: 19:07
 */

namespace Pixasia\Notification;

interface GatewayInterface
{

    public function send();
}