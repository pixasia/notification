<?php
/**
 * Created by PhpStorm.
 * User: Sammy
 * Date: 04/01/2017
 * Time: 18:38
 */

namespace Pixasia\Notification\Gateway;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Exception\RequestException;

use Pixasia\Notification\Helper;


/**
 * Class Facebook
 * @package Pixasia\Notification\Gateway
 *
 * @method getApp_Id
 * @method getSecret
 * @method getUser
 * @method getMessage
 * @method setApp_Id
 * @method setSecret
 * @method setUser
 * @method setMessage
 */
class Facebook implements \Pixasia\Notification\GatewayInterface
{

    use Helper\MagicSetterTrait;
    use Helper\NotificationTrait;

    /**
     * API URL to use
     */
    const API_URL = 'https://graph.facebook.com/%s/notifications';

    /**
     * @var array[string] $requires An array of the required variables to send a notification
     */
    protected $requires = [
      'app_id',
      'secret',
      'user',
      'message'
    ];


    /**
     * Facebook constructor.
     *
     * @param array $data
     * @param Client $client
     */
    public function __construct($data = [], $client = null)
    {
        $this->import($data);
        $this->setClient($client);
    }

    /**
     * Get the App token
     *
     * @return string
     */
    private function getAppToken()
    {
        return $this->getApp_id() . '|' . $this->getSecret();
    }

    /**
     * Build the required data
     *
     * @return array
     */
    private function getData()
    {
        return [
          'access_token' => $this->getAppToken(),
          'template' => $this->getMessage()
        ];
    }

    /**
     * Send the Facebook notification
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function send()
    {
        $this->checkRequirements();

        $this->setUrl(sprintf(self::API_URL, $this->getUser()));

        $client = $this->getClient();

        try {
            $response = $client->post($this->getURL(), [
              'form_params' => $this->getData()
            ]);
        } catch (RequestException $e) {
            if ($e->getResponse()->getStatusCode() == 500) {
                throw new \Exception('Something went wrong with the request');
            }

            $error = json_decode($e->getResponse()->getBody());
            throw new \Exception($error->error->message);
        }

        return ($response->getStatusCode() == 200) ? true : false;
    }
}