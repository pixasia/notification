<?php
/**
 * Created by PhpStorm.
 * User: Sammy
 * Date: 04/01/2017
 * Time: 14:14
 */
namespace Pixasia\Notification\Gateway;

use GuzzleHttp\Exception\RequestException;

use Pixasia\Notification\Helper;

/**
 * Class Slack
 *
 * Notify a Slack App which can be created from here: https://api.slack.com/apps?new_app=1
 *
 * @package Pixasia\Notification\Gateway
 */
class Slack implements \Pixasia\Notification\GatewayInterface
{

    /**
     * The api endpoint.
     */
    const API_URL = 'https://slack.com/api/chat.postMessage';

    /**
     * An array of required variables
     *
     * @var array
     */
    private $requires = [
      'token',
      'message'
    ];

    use Helper\MagicSetterTrait;
    use Helper\NotificationTrait;


    /**
     * Slack constructor.
     *
     * @param array $data
     * @param \GuzzleHttp\Client $client
     */
    public function __construct($data = [], $client = null)
    {
        $this->import($data);
        $this->setClient($client);
    }

    /**
     * Build an array of form params to be sent to Slack
     *
     * @return array
     */
    private function getData()
    {
        return [
          'unfurl_links' => true,
          'token' => $this->getToken(),
          'username' => $this->getFrom(),
          'as_user' => $this->getUser(),
          'channel' => $this->getChannel(),
          'text' => $this->getMessage(),
        ];
    }

    /**
     * Send the notification
     *
     * @return bool
     * @throws \Exception
     */
    public function send()
    {
        $this->checkRequirements();

        $client = $this->getClient();

        try {
            $response = $client->post(self::API_URL, [
              'exceptions' => true,
              'timeout' => '80',
              'connect_timeout' => '30',
              'headers' => [
                'Accept' => 'application/json',
              ],
              'form_params' => $this->getData()
            ]);

            $response = json_decode($response->getBody(), true);
            $success = $response['ok'];

            if ($success) {
                return true;
            } else {
                throw new \Exception($response['error']);
            }

        } catch (RequestException $e) {
            if ($e->getResponse()->getStatusCode() == 500) {
                throw new \Exception('Something went wrong with the request');
            }

            $error = json_decode($e->getResponse()->getBody());
            throw new \Exception($error->error->message);
        }
    }
}