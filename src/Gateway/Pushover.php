<?php
/**
 * Created by PhpStorm.
 * User: Sammy
 * Date: 03/01/2017
 * Time: 19:06
 */

namespace Pixasia\Notification\Gateway;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Exception\RequestException;
use Pixasia\Notification\Helper\MagicSetterTrait;
use Pixasia\Notification\Helper\NotificationTrait;


/**
 * Class Pushover
 * @package Pixasia\Notification\Gateway
 *
 * @property    $token
 * @property    $user
 * @property    $title
 * @property    $message
 * @property    $html
 * @property    $device
 * @property    $priority
 * @property    $timestamp
 * @property    $expire
 * @property    $retry
 * @property    $callback
 * @property    $url
 * @property    $sound
 * @property    $url_title
 *
 * @method getToken
 * @method getUser
 * @method getTitle
 * @method getMessage
 * @method getHtml
 * @method getDevice
 * @method getPriority
 * @method getExpire
 * @method getRetry
 * @method getCallback
 * @method getUrl
 * @method getSound
 * @method getUrlTitle
 * @method getTimestamp
 * @method setToken
 * @method setUser
 * @method setTitle
 * @method setMessage
 * @method setHtml
 * @method setDevice
 * @method setPriority
 * @method setExpire
 * @method setRetry
 * @method setCallback
 * @method setUrl
 * @method setSound
 * @method setUrlTitle
 * @method setTimestamp
 */
class Pushover implements \Pixasia\Notification\GatewayInterface
{

    use MagicSetterTrait;
    use NotificationTrait;

    /**
     * API_URL
     */
    const API_URL = 'https://api.pushover.net/1/messages.json';

    /**
     *
     */
    const VALIDATION_URL = 'https://api.pushover.net/1/users/validate.json';

    /**
     *
     */
    const SOUNDS_URL = 'https://api.pushover.net/1/sounds.json?token=%s';

    /**
     * @var array $requires An array of required parameters
     */
    protected $requires = [
      'token',
      'user',
      'message'
    ];


    /**
     * Default constructor
     *
     * @param array [string] $data Array of data to initialise
     * @param Client $client
     */
    public function __construct($data = [], $client = null)
    {
        $this->import($data);
        $this->setClient($client);
    }

    /**
     * Return a list of params for the notification
     *
     * @return array
     */
    private function getData()
    {
        return [
          'token' => $this->getToken(),
          'user' => $this->getUser(),
          'title' => $this->getTitle(),
          'message' => $this->getMessage(),
          'html' => $this->getHtml(),
          'device' => $this->getDevice(),
          'priority' => $this->getPriority(),
          'timestamp' => $this->getTimestamp(),
          'expire' => $this->getExpire(),
          'retry' => $this->getRetry(),
          'callback' => $this->getCallback(),
          'url' => $this->getUrl(),
          'sound' => $this->getSound(),
          'url_title' => $this->getUrlTitle()
        ];
    }

    /**
     * Handles the errors thrown by Guzzle
     *
     * @param RequestException $e
     *
     * @return string
     */
    private function handleError($e)
    {
        if ($e->getResponse()->getStatusCode() == 500) {
            return 'Something went wrong with the request';
        }

        $error = json_decode($e->getResponse()->getBody());
        return $error->errors[0];
    }

    /**
     * Validate the user using Pushover API
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function validateUser()
    {
        $this->checkRequirements();

        $client = $this->getClient();

        try {
            $response = $client->post(self::VALIDATION_URL, [
              'form_params' => [
                "token" => $this->getToken(),
                "user" => $this->getUser()
              ]
            ]);
        } catch (RequestException $e) {
            throw new \Exception($this->handleError($e));
        }

        $json = json_decode($response->getBody());

        return $json->status;
    }


    /**
     * Gets an array of notification sounds that can be used with Pushover.
     *
     * @return array
     * @throws \Exception
     */
    public function getSoundsList()
    {
        if ($this->getToken() != null) {
            $client = $this->getClient();
            try {
                $response = $client->get(sprintf(self::SOUNDS_URL, $this->getToken()));

                $decoded = json_decode($response->getBody()->getContents(), true);
                return $decoded['sounds'];
            } catch (RequestException $e) {
                throw new \Exception($this->handleError($e));
            }
        } else {
            throw new \Exception('You must supply a Pushover token to retrieve the sound list');
        }
    }

    /**
     * Send message to Pushover API
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function send()
    {
        $this->checkRequirements();

        if (!isset($this->timestamp)) {
            $this->setTimestamp(time());
        }

        $client = $this->getClient();

        try {
            $response = $client->post(self::API_URL, [
              'form_params' => $this->getData()
            ]);
        } catch (RequestException $e) {
            throw new \Exception($this->handleError($e));
        }

        return ($response->getStatusCode() == 200);
    }
}

?>