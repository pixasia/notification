<?php
/**
 * Created by PhpStorm.
 * User: Sammy
 * Date: 04/01/2017
 * Time: 18:57
 */

namespace Pixasia\Notification\Helper;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Exception\RequestException;

trait NotificationTrait
{

    /**
     * @var $_client \GuzzleHttp\Client
     */
    protected $_client;

    /**
     * Update the private client method
     *
     * @param $client \GuzzleHttp\Client
     */
    public function setClient($client)
    {
        $this->_client = $client;
    }

    /**
     * @return \GuzzleHttp\Client
     */
    public function getClient()
    {
        if ($this->_client !== null) {
            return $this->_client;
        } else {
            return $this->_client = new Client([
              'timeout' => 30
            ]);
        }
    }

    /**
     * Check the gateways requirements before sending
     *
     * @return bool
     * @throws \Exception
     */
    public function checkRequirements()
    {
        if (!isset($this->requires)) return true;

        foreach ($this->requires as $requirement) {
            if (!isset($this->$requirement)) {
                throw new \Exception('You must set a ' . $requirement);
            }
        }

        return true;
    }
}