<?php
/**
 * Created by PhpStorm.
 * User: Sammy
 * Date: 04/01/2017
 * Time: 19:10
 */

namespace Pixasia\Notification\Helper;

trait MagicSetterTrait
{

    /**
     * @var array[string] $_data An array of all the stored data
     */
    protected $_data;

    /**
     * Import an array of data magically.
     *
     * @param array $data The array of data to import
     */
    public function import($data = [])
    {
        foreach ($data as $key => $value) {
            $this->__set($key, $value);
        }
    }

    /**
     * Set a variable
     *
     * @param string $name
     * @param $value
     */
    public function __set($name, $value)
    {
        $this->_data[strtolower($name)] = $value;
    }

    /**
     * Return a set variable
     *
     * @param string $name
     * @return null
     */
    public function __get($name)
    {
        return isset($this->_data[strtolower($name)]) ? $this->_data[strtolower($name)] : null;
    }

    /**
     * Check if a variable is set
     * @param string $name
     * @return bool
     */
    public function __isset($name)
    {
        return isset($this->_data[strtolower($name)]);
    }

    /**
     * Magic function to allow set + get functions to exist
     *
     * @param $name
     * @param $arguments
     * @return null
     * @throws \Exception
     */
    public function __call($name, $arguments)
    {
        $method = substr($name, 0, 3);
        $s = substr($name, 3);

        switch ($method) {
            case 'get':
                return $this->__get($s);
                break;
            case 'set':
                $this->__set($s, $arguments[0]);
                break;
            default:
                throw new \Exception('Undefined method ' . $name);
        }
    }

    /**
     * Return all set data
     *
     * @return array[string]
     */
    public function getAll()
    {
        return $this->_data;
    }
}