<?php

use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Exception\RequestException;

class PushoverTest extends \PHPUnit_Framework_TestCase
{

    const TOKEN = '123abc';
    const USER = 'USER123';
    const MESSAGE = 'TESTING';

    /**
     * Return a generated Pushover client
     *
     * @param null $client
     *
     * @return \Pixasia\Notification\Gateway\Pushover
     * @throws Exception
     */
    public static function getPushover($client = null)
    {
        return \Pixasia\Notification\Factory::create('pushover', [], $client);
    }

    /**
     * @covers Pixasia\Notification\Gateway\Pushover __construct
     *
     * @covers Pixasia\Notification\Helper\MagicSetterTrait __set
     * @covers Pixasia\Notification\Helper\MagicSetterTrait __get
     */
    public function testConstruct()
    {
        $pushover = Pixasia\Notification\Factory::create('pushover', [
          'token' => self::TOKEN,
          'user' => self::USER,
          'message' => self::MESSAGE
        ]);

        $this->assertEquals(self::TOKEN, $pushover->getToken());
        $this->assertEquals($pushover->getUser(), self::USER);
        $this->assertEquals($pushover->getMessage(), self::MESSAGE);
    }

    /**
     * @covers Pixasia\Notification\Gateway\Pushover setToken
     * @covers Pixasia\Notification\Gateway\Pushover getToken
     */
    public function testSetGetToken()
    {
        $pushover = self::getPushover();

        $token = 'foo';
        $pushover->setToken($token);

        $this->assertEquals($pushover->getToken(), $token);
    }

    /**
     * @covers Pixasia\Notification\Gateway\Pushover setUser
     * @covers Pixasia\Notification\Gateway\Pushover getUser
     */
    public function testSetGetUser()
    {
        $pushover = self::getPushover();

        $token = 'foo';
        $pushover->setUser($token);

        $this->assertEquals($pushover->getUser(), $token);
    }

    /**
     * @covers Pixasia\Notification\Gateway\Pushover setTitle
     * @covers Pixasia\Notification\Gateway\Pushover getTitle
     */
    public function testSetGetTitle()
    {
        $pushover = self::getPushover();

        $token = 'foo';
        $pushover->setTitle($token);

        $this->assertEquals($pushover->getTitle(), $token);
    }

    /**
     * @covers Pixasia\Notification\Gateway\Pushover setHtml
     * @covers Pixasia\Notification\Gateway\Pushover getHtml
     */
    public function testSetGetHtml()
    {
        $pushover = self::getPushover();

        $token = true;
        $pushover->setHtml($token);

        $this->assertEquals($pushover->getHtml(), $token);
    }

    /**
     * @covers Pixasia\Notification\Gateway\Pushover setRetry
     * @covers Pixasia\Notification\Gateway\Pushover getRetry
     */
    public function testSetGetRetry()
    {
        $pushover = self::getPushover();

        $token = 35;
        $pushover->setRetry($token);

        $this->assertEquals($pushover->getRetry(), $token);
    }

    /**
     * @covers Pixasia\Notification\Gateway\Pushover setExpire
     * @covers Pixasia\Notification\Gateway\Pushover getExpire
     */
    public function testSetGetExpire()
    {
        $pushover = self::getPushover();

        $token = 3600;
        $pushover->setExpire($token);

        $this->assertEquals($pushover->getExpire(), $token);
    }

    /**
     * @covers Pixasia\Notification\Gateway\Pushover setCallback
     * @covers Pixasia\Notification\Gateway\Pushover getCallback
     */
    public function testSetGetCallback()
    {
        $pushover = self::getPushover();

        $token = 'http://google.com/';
        $pushover->setCallback($token);

        $this->assertEquals($pushover->getCallback(), $token);
    }

    /**
     * @covers Pixasia\Notification\Gateway\Pushover setMessage
     * @covers Pixasia\Notification\Gateway\Pushover getMessage
     */
    public function testSetGetMessage()
    {
        $pushover = self::getPushover();

        $token = 'foo';
        $pushover->setMessage($token);

        $this->assertEquals($pushover->getMessage(), $token);
    }

    /**
     * @covers Pixasia\Notification\Gateway\Pushover setDevice
     * @covers Pixasia\Notification\Gateway\Pushover getDevice
     */
    public function testSetGetDevice()
    {
        $pushover = self::getPushover();

        $token = 'foo';
        $pushover->setDevice($token);

        $this->assertEquals($pushover->getDevice(), $token);
    }

    /**
     * @covers Pixasia\Notification\Gateway\Pushover setTimestamp
     * @covers Pixasia\Notification\Gateway\Pushover getTimestamp
     */
    public function testSetGetTimestamp()
    {
        $pushover = self::getPushover();

        $token = 3600;
        $pushover->setTimestamp($token);

        $this->assertEquals($pushover->getTimestamp(), $token);
    }

    /**
     * @covers Pixasia\Notification\Gateway\Pushover setPriority
     * @covers Pixasia\Notification\Gateway\Pushover getPriority
     */
    public function testSetGetPriority()
    {
        $pushover = self::getPushover();

        $token = 1;
        $pushover->setPriority($token);

        $this->assertEquals($pushover->getPriority(), $token);
    }

    /**
     * @covers Pixasia\Notification\Gateway\Pushover setUrl
     * @covers Pixasia\Notification\Gateway\Pushover getUrl
     */
    public function testSetGetUrl()
    {
        $pushover = self::getPushover();

        $token = 'foo';
        $pushover->setUrl($token);

        $this->assertEquals($pushover->getUrl(), $token);
    }

    /**
     * @covers Pixasia\Notification\Gateway\Pushover setUrlTitle
     * @covers Pixasia\Notification\Gateway\Pushover getUrlTitle
     */
    public function testSetGetUrlTitle()
    {
        $pushover = self::getPushover();

        $token = 'foo';
        $pushover->setUrlTitle($token);

        $this->assertEquals($pushover->getUrlTitle(), $token);
    }

    /**
     * @covers Pixasia\Notification\Gateway\Pushover setDebug
     * @covers Pixasia\Notification\Gateway\Pushover getDebug
     */
    public function testSetGetDebug()
    {
        $pushover = self::getPushover();

        $token = true;
        $pushover->setDebug($token);

        $this->assertEquals($pushover->getDebug(), $token);
    }

    /**
     * @covers Pixasia\Notification\Gateway\Pushover setSound
     * @covers Pixasia\Notification\Gateway\Pushover getSound
     */
    public function testSetGetSound()
    {
        $pushover = self::getPushover();

        $token = 'foo';
        $pushover->setSound($token);

        $this->assertEquals($pushover->getSound(), $token);
    }

    /**
     * @covers Pixasia\Notification\Gateway\Pushover send
     *
     * @expectedException \Exception
     */
    public function testServerError()
    {
        $mock = new MockHandler([
          new Response(500, ['X-Foo' => 'Bar'])
        ]);
        $handler = HandlerStack::create($mock);
        $client = new Client(['handler' => $handler]);

        //Create a pushover client with a erorr handler.
        $pushover = self::getPushover($client);
        $pushover->setToken(self::TOKEN);
        $pushover->setUser(self::USER);
        $pushover->setMessage(self::MESSAGE);

        $pushover->send();
    }

    /**
     * @covers Pixasia\Notification\Gateway\Pushover send
     *
     * @expectedException \Exception
     * @expectedExceptionMessage message cannot be blank
     */
    public function testInvalidData()
    {
        $mock = new MockHandler([
          new Response(400, [], '{"message":"cannot be blank","errors":["message cannot be blank"],"status":0}')
        ]);
        $handler = HandlerStack::create($mock);
        $client = new Client(['handler' => $handler]);

        //Create a pushover client with a erorr handler.
        $pushover = self::getPushover($client);
        $pushover->setToken(self::TOKEN);
        $pushover->setUser(self::USER);
        $pushover->setMessage(self::MESSAGE);

        $pushover->send();
    }

    /**
     * @covers Pixasia\Notification\Gateway\Pushover send
     * @covers Pixasia\Notification\Helper\NotificationTrait getClient
     */
    public function testSuccessfullySent()
    {
        $mock = new MockHandler([
          new Response(200)
        ]);
        $handler = HandlerStack::create($mock);
        $client = new Client(['handler' => $handler]);

        //Create a pushover client with a erorr handler.
        $pushover = self::getPushover($client);
        $pushover->setToken(self::TOKEN);
        $pushover->setUser(self::USER);
        $pushover->setMessage(self::MESSAGE);

        $this->assertTrue($pushover->send());
    }

    /**
     * @covers Pixasia\Notification\Helper\NotificationTrait    checkRequirements
     *
     * @expectedException \Exception
     */
    public function testNoData()
    {
        $pushover = self::getPushover();
        $pushover->send();
    }

    /**
     * Validate a user successfully
     */
    public function testValidateUser()
    {
        $mock = new MockHandler([
          new Response(200, [], '{"status":true}')
        ]);
        $handler = HandlerStack::create($mock);
        $client = new Client(['handler' => $handler]);

        //Create a pushover client with a erorr handler.
        $pushover = self::getPushover($client);
        $pushover->setToken(self::TOKEN);
        $pushover->setUser(self::USER);
        $pushover->setMessage(self::MESSAGE);

        $this->assertTrue($pushover->validateUser());
    }

    /**
     * Handle a server error whilst validating a user
     *
     * @expectedException \Exception
     */
    public function testHandleValidateUserServerError()
    {
        $mock = new MockHandler([
          new Response(500, [], '{"status":true}')
        ]);
        $handler = HandlerStack::create($mock);
        $client = new Client(['handler' => $handler]);

        //Create a pushover client with a erorr handler.
        $pushover = self::getPushover($client);
        $pushover->setToken(self::TOKEN);
        $pushover->setUser(self::USER);
        $pushover->setMessage(self::MESSAGE);

        $this->assertTrue($pushover->validateUser());
    }

    /**
     * Handle an invalid request whilst validating a user
     *
     * @expectedException \Exception
     * @expectedExceptionMessage token cannot be blank
     */
    public function testHandleValidateUserInvalidRequest()
    {
        $mock = new MockHandler([
          new Response(400, [], '{"message":"token be blank","errors":["token cannot be blank"],"status":0}')
        ]);
        $handler = HandlerStack::create($mock);
        $client = new Client(['handler' => $handler]);

        //Create a pushover client with a erorr handler.
        $pushover = self::getPushover($client);
        $pushover->setToken(self::TOKEN);
        $pushover->setUser(self::USER);
        $pushover->setMessage(self::MESSAGE);

        $this->assertTrue($pushover->validateUser());
    }

    /**
     * Test getting a list of sounds returns an array
     */
    public function testGetSounds()
    {
        $mock = new MockHandler([
          new Response(200, [], '{"sounds":{"pushover":"Pushover (default)","bike":"Bike","bugle":"Bugle","cashregister":"Cash Register","classical":"Classical","cosmic":"Cosmic","falling":"Falling","gamelan":"Gamelan","incoming":"Incoming","intermission":"Intermission","magic":"Magic","mechanical":"Mechanical","pianobar":"Piano Bar","siren":"Siren","spacealarm":"Space Alarm","tugboat":"Tug Boat","alien":"Alien Alarm (long)","climb":"Climb (long)"}}')
        ]);
        $handler = HandlerStack::create($mock);
        $client = new Client(['handler' => $handler]);

        $pushover = self::getPushover($client);
        $pushover->setToken(self::TOKEN);
        $pushover->setUser(self::USER);
        $pushover->setMessage(self::MESSAGE);

        $this->assertInternalType('array', $pushover->getSoundsList());
    }

    /**
     * Test that getting a list of sounds without a token throws an error
     *
     * @expectedException \Exception
     * @expectedExceptionMessage You must supply a Pushover token to retrieve the sound list
     */
    public function testGetSoundsWithoutToken()
    {
        $pushover = self::getPushover();
        $pushover->getSoundsList();
    }

    /**
     * Test that getting a list of sounds without a token throws an error
     *
     * @expectedException \Exception
     * @expectedExceptionMessage token cannot be blank
     */
    public function testGetSoundsWithError()
    {
        $mock = new MockHandler([
          new Response(400, [], '{"message":"token be blank","errors":["token cannot be blank"],"status":0}')
        ]);
        $handler = HandlerStack::create($mock);
        $client = new Client(['handler' => $handler]);

        $pushover = self::getPushover($client);
        $pushover->setToken(self::TOKEN);
        $pushover->getSoundsList();
    }
}