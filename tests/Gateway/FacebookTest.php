<?php

use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Exception\RequestException;

class FacebookTest extends \PHPUnit_Framework_TestCase
{

    const APP_ID = 'app_id';
    const SECRET = 'secret';
    const USER = 'USER123';
    const MESSAGE = 'TESTING';

    /**
     * What notification handler to use
     */
    const NOTIFICATION = 'facebook';

    /**
     * @param null $client
     * @param array $data
     * @return \Pixasia\Notification\GatewayInterface
     * @throws Exception
     */
    public static function getNotification($client = null, $data = [])
    {
        return \Pixasia\Notification\Factory::create(self::NOTIFICATION, $data, $client);
    }

    /**
     * @covers Pixasia\Notification\Gateway\Facebook __construct
     */
    public function testConstruct()
    {
        $notification = Pixasia\Notification\Factory::create(self::NOTIFICATION, [
          'app_id' => self::APP_ID,
          'user' => self::USER,
          'message' => self::MESSAGE
        ]);

        $this->assertEquals(self::APP_ID, $notification->getApp_Id());
        $this->assertEquals($notification->getUser(), self::USER);
        $this->assertEquals($notification->getMessage(), self::MESSAGE);
    }

    /**
     * @covers Pixasia\Notification\Helper\NotificationTrait    checkRequirements
     *
     * @expectedException \Exception
     */
    public function testNoData()
    {
        $notification = self::getNotification();
        $notification->send();
    }

    /**
     * @covers Pixasia\Notification\Gateway\Facebook send
     *
     * @expectedException \Exception
     * @expectedExceptionMessage Something went wrong with the request
     */
    public function testServerError()
    {
        $mock = new MockHandler([
          new Response(500, ['X-Foo' => 'Bar'])
        ]);
        $handler = HandlerStack::create($mock);
        $client = new Client(['handler' => $handler]);

        //Create a Facebook client with a error handler.
        $notification = self::getNotification($client, [
          'app_id' => self::APP_ID,
          'secret' => self::SECRET,
          'user' => self::USER,
          'message' => self::MESSAGE
        ]);

        $notification->send();
    }

    /**
     * @covers Pixasia\Notification\Gateway\Facebook send
     *
     * @expectedException \Exception
     * @expectedExceptionMessage Invalid OAuth access token signature.
     */
    public function testInvalidData()
    {
        $mock = new MockHandler([
          new Response(400, [], '{"error":{"message":"Invalid OAuth access token signature.","type":"OAuthException","code":190}}')
        ]);
        $handler = HandlerStack::create($mock);
        $client = new Client(['handler' => $handler]);

        //Create a Facebook client with a error handler.
        $notification = self::getNotification($client, [
          'app_id' => self::APP_ID,
          'secret' => self::SECRET,
          'user' => self::USER,
          'message' => self::MESSAGE
        ]);

        $notification->send();
    }


    /**
     * @covers Pixasia\Notification\Gateway\Facebook send
     * @covers Pixasia\Notification\Helper\NotificationTrait getClient
     */
    public function testSuccessfullySent()
    {
        $mock = new MockHandler([
          new Response(200)
        ]);
        $handler = HandlerStack::create($mock);
        $client = new Client(['handler' => $handler]);

        //Create a Facebook client with a error handler.
        $notification = self::getNotification($client, [
          'app_id' => self::APP_ID,
          'secret' => self::SECRET,
          'user' => self::USER,
          'message' => self::MESSAGE
        ]);

        $this->assertTrue($notification->send());
    }

}