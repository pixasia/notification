<?php

/**
 * Created by PhpStorm.
 * User: Sammy
 * Date: 03/01/2017
 * Time: 19:29
 */
class FactoryTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @expectedException Exception
     */
    public function testInvalidClass()
    {
        $factory = \Pixasia\Notification\Factory::create('invalid');
    }

    /**
     * @return Pixasia\Notification\Gateway\Pushover;
     */
    public function testValidClass()
    {
        return \Pixasia\Notification\Factory::create('pushover');
    }
}