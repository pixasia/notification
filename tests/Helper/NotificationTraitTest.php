<?php
/**
 * Created by PhpStorm.
 * User: Sammy
 * Date: 05/01/2017
 * Time: 11:04
 */

use GuzzleHttp\Client;

/**
 * Class NotificationTraitTest
 *
 * A test class for testing the notification trait
 */
class NotificationTraitTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @return Mock;
     */
    private function getTraitMock()
    {
        return $this->getMockForTrait('\\Pixasia\\Notification\\Helper\\NotificationTrait');
    }

    /**
     * Test the magic getters and setters for the client
     */
    public function testSetterGetter()
    {
        $mock = $this->getTraitMock();

        $client = new Client();
        $mock->setClient($client);

        $this->assertEquals($client, $mock->getClient());
        $this->assertInstanceOf('\\GuzzleHttp\\Client', $mock->getClient());
    }

    /**
     * Ensure that if a client isn't set, then a new one is returned
     */
    public function testCreateClient()
    {
        $mock = $this->getTraitMock();

        $this->assertInstanceOf('\\GuzzleHttp\\Client', $mock->getClient());
    }

    /**
     * Check that if no requirements are set, then it returns true
     */
    public function testEmptyRequirements()
    {
        $mock = $this->getTraitMock();
        $mock->requires = [];

        $this->assertEquals(true, $mock->checkRequirements());
    }

    /**
     * Check that if a requirement hasn't been set, that it throws an exception
     *
     * @expectedException \Exception
     * @expectedExceptionMessage You must set a token
     */
    public function testMissingRequirements()
    {
        $mock = $this->getTraitMock();
        $mock->requires = ['token'];

        $this->assertEquals(true, $mock->checkRequirements());
    }

    /**
     * Check that if a requirement has been set, no errors occur
     */
    public function testFullRequirements()
    {
        $mock = $this->getTraitMock();
        $mock->requires = ['token'];

        $mock->token = 'foo';

        $this->assertEquals(true, $mock->checkRequirements());
    }

}