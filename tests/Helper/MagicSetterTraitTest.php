<?php
/**
 * Created by PhpStorm.
 * User: Sammy
 * Date: 04/01/2017
 * Time: 20:14
 */

/**
 * Class MagicSetterTraitTest
 *
 * A test class for testing the magic setter and getter trait
 */
class MagicSetterTraitTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @return Mock;
     */
    private function getTraitMock()
    {
        return $this->getMockForTrait('\\Pixasia\\Notification\\Helper\\MagicSetterTrait');
    }

    /**
     * @expectedException \Exception
     * @expectedExceptionMessage Undefined method putSomething
     */
    public function testUndefinedMethod()
    {
        $mock = $this->getTraitMock();

        $mock->putSomething();
    }

    /**
     * Test the magic getters and setters
     */
    public function testSetterGetter()
    {
        $mock = $this->getTraitMock();

        $foo = 'bar';
        $mock->setTest($foo);

        $this->assertEquals($foo, $mock->getTest());
    }

    /**
     * Test the __isset magic handler
     */
    public function testIsset()
    {
        $mock = $this->getTraitMock();

        $this->assertFalse(isset($mock->foo));

        $mock->setFoo(true);

        $this->assertTrue(isset($mock->foo));
    }

    /**
     * Test all data is returned
     */
    public function testGetAll()
    {
        $mock = $this->getTraitMock();

        $this->assertEquals(0, count($mock->getAll()));

        $mock->setFoo(true);
        $mock->setBar(false);

        $this->assertEquals(2, count($mock->getAll()));
    }

    /**
     * Test importing data
     */
    public function testImport()
    {
        $mock = $this->getTraitMock();

        $mock->import([
          'foo' => 'bar',
          'x' => 'j'
        ]);

        $this->assertEquals(2, count($mock->getAll()));

        $this->assertEquals('bar', $mock->getFoo());
        $this->assertEquals('j', $mock->getX());
    }
}