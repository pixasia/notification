#Notifications

This a PHP package to allow you to easily send notifications to your favourite outlet.

##Setup
As this package hasn't been released yet, you must add it to your composer.json manually.

First, add the following snippet to your composer.json. (If you already have repositories, just add the content)

```JSON
	"repositories": [
        {
          "url": "https://samtidball@bitbucket.org/pixasia/notification.git",
          "type": "git"
        }
	],
```

Next, we need to actually require the package. This will tell composer that it needs to download it.

```JSON
 "require": {
    "pixasia/queue": "dev-master"
 }
```

Finally run a composer update to install the package.

##Usage

###Pushover

This is a test example of how to send a Pushover notification

```PHP
 $pushover = \Pixasia\Notification\Factory::create('pushover', [
      'token' => YOUR_PUSHOVER_TOKEN,
      'user'  => YOUR_PUSHOVER_USER,
      'message' => 'Hi, this is a test message'
    ]);

 $pushover->send();
```

###Slack

This is a test example of how to send a Slack notification

```PHP
   $slack = \Pixasia\Notification\Factory::create('slack', [
      'token' => YOUR_SLACK_TOKEN,
      'channel'   =>  YOUR_SLACK_CHANNEL,
      'message' => 'Hi, this is a test message'
    ]);

  $slack->send();
```

###Facebook

This is an example of how to send a Facebook notification

```PHP
   $facebook = \Pixasia\Notification\Factory::create('facebook', [
      'app_id' => YOUR_APP_ID,
      'secret' => YOUR_APP_SECRET,
      'user'   => YOUR_FACEBOOK_USER_ID,
      'message' => 'Hi, this is a test message'
    ]);

  $facebook->send();
```

##Examples

 * Pushover - Send a notification to your users via [Pushover](https://pushover.net/)
 * Slack - Send a notification to a channel on [Slack](https://slack.com)
 * Facebook Notification - Send a desktop notification to a Facebook user using Canvas


##License
The MIT License (MIT)

Copyright (c) 2016-2017 Sam Tidball - Pixedo

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.